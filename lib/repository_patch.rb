module RepositoryPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      after_create 'call_hook :create'
      after_update 'call_hook :update'
      after_destroy 'call_hook :destroy'
    end
  end

  module InstanceMethods

    def call_hook(hook)
      cmd  = Setting.plugin_repository_creator[:hooks][scm_name][hook]
      user = Setting.plugin_repository_creator[:hook_user]

      unless cmd.blank?
        cmd.sub!(/ \$repo/,  " #{url}")
        cmd.sub!(/ \$proj/,  " #{project.name}")
        cmd.sub!(/ \$id/,    " #{project.id}")
        contact = project.principals.first.mail unless project.principals.empty?
        cmd.sub!(/ \$princ/, " #{contact}")
        cmd.sub!(/ \$url/,   " #{Setting.protocol}://#{Setting.host_name}")
        cmd.sub!(/ \$api/,   " #{Setting.sys_api_key}")

        if user and !user.blank?
          Rails.logger.info "Create repository with command 'sudo -u #{user} #{cmd}"
          `sudo -u #{user} #{cmd}`
        else
          Rails.logger.info "Create repository with command '#{cmd}"
          `#{cmd}`
        end
      end
    end
    private :call_hook

  end
end

Repository.send(:include, RepositoryPatch)