module RepositoriesControllerPatch

  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      before_filter :make_url_from_params, :only => [:create, :update]
    end
  end

  module InstanceMethods

    def make_url_from_params
      base_path = Setting.plugin_repository_creator[:base_paths][params[:repository_scm]]
      identifier = @project.identifier
      unless params[:repository]['identifier'].empty?
        identifier += '.' + params[:repository]['identifier']
      end
      params[:repository_url] = File.expand_path identifier, base_path                                 
    end
  
  end

end

RepositoriesController.send(:include, RepositoriesControllerPatch)