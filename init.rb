require 'repository_patch'
require 'git_adapter_patch'
require 'mercurial_adapter_patch'
require 'repositories_controller_patch'

Redmine::Plugin.register :repository_creator do
  name 'Repository Creator plugin'
  author 'Michael Lamberti'
  description 'A redmine plugin to automatically create repositories in a given basedir'
  version '0.0.1'
  url 'http://bitbucket.org/knutmithut/repository_creator'
  author_url ''

  base_paths = {}
  hooks = {}
  Redmine::Scm::Base.all.each do |scm|
    cmd = "Repository::#{scm}".constantize.scm_command
    base_paths[scm] = File.expand_path("../#{cmd}/", Rails.root)
    hooks[scm] = {
      :create => File.expand_path("plugins/repository_creator/lib/hooks/#{cmd}_create $proj $repo $princ $url $api", Rails.root),
      :update => File.expand_path("plugins/repository_creator/lib/hooks/#{cmd}_update $proj $repo $princ $url $api", Rails.root),
      :destroy => File.expand_path("plugins/repository_creator/lib/hooks/#{cmd}_destroy $proj $repo $princ $url $api", Rails.root)
    }
  end

  settings({
    :partial => 'settings/repository_creator_settings',
    :default => {
      :base_paths => base_paths,
      :separator => '.',
      :hook_user => '',
      :hooks => hooks
    }
  })

end
